package se.codkingkoi.lakitu

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import se.codkingkoi.lakitu.api.WeatherData
import java.util.Calendar

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val tempViewModel: TemperatureViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // This is to draw edge-to-edge under both the nav bar and status bar
        mainView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)

        setUpChart()

        tempViewModel.temperatures.observe(this, Observer { weatherData ->
            setChartData(weatherData)
        })
    }

    private fun setUpChart() {
        chart.description.isEnabled = false
        chart.setDrawGridBackground(false)

        val x = chart.xAxis
        x.position = XAxis.XAxisPosition.BOTTOM
        x.axisLineColor = Color.WHITE
        x.textColor = Color.WHITE
        x.setDrawGridLines(false)
        x.setDrawAxisLine(false)
        x.labelCount = 9
        x.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return "${value.toInt()}:00"
            }
        }

        val y = chart.axisLeft
        y.textColor = Color.WHITE
        y.setDrawGridLines(true)
        y.setDrawAxisLine(false)
        y.gridColor = Color.WHITE
        y.axisLineColor = Color.WHITE
        y.valueFormatter = object : ValueFormatter() {

            override fun getFormattedValue(value: Float): String {
                return "${value.toInt()} °C"
            }
        }

        chart.axisRight.isEnabled = false
        chart.legend.isEnabled = false
    }

    private fun setChartData(weatherData: WeatherData?) {
        val values = weatherData?.entries?.map {
            val calendar = Calendar.getInstance()
            calendar.time = it.time
            Entry(calendar[Calendar.HOUR_OF_DAY].toFloat(), it.temperature ?: 0f)
        } ?: emptyList()

        val set = LineDataSet(values, "Temperature")

        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.setDrawCircles(false)
        set.lineWidth = 2f
        set.color = Color.WHITE

        val lineData = LineData(set)
        lineData.setDrawValues(false)

        chart.data = lineData
        chart.invalidate()
    }
}
