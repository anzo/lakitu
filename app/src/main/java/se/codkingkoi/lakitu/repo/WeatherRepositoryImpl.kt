package se.codkingkoi.lakitu.repo

import android.util.Log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import retrofit2.HttpException
import se.codkingkoi.lakitu.api.BackendService
import se.codkingkoi.lakitu.api.BackendService.Credentials
import se.codkingkoi.lakitu.api.WeatherData

class WeatherRepositoryImpl(private val backendService: BackendService) : WeatherRepository {

    private var token: String = ""

    override suspend fun getWeatherData(): Flow<WeatherData?> {
        if (token.isEmpty()) {
            fetchToken()
        }

        val data = try {
            backendService.getTemperatures(token, BackendService.Query(QUERY))
        } catch (e: HttpException) {
            Log.w(TAG, "Couldn't fetch the data", e)

            if (e.code() == UNAUTHORIZED) {
                fetchToken()
                backendService.getTemperatures(token, BackendService.Query(QUERY))
            } else {
                null
            }
        } catch (e: Exception) {
            Log.w(TAG, "Got some other error while fetching the data", e)
            null
        }

        return flowOf(data)
    }

    private suspend fun fetchToken() {
        val response = backendService.getToken(Credentials(EMAIL, PASSWORD))
        if (response.isSuccessful) {
            token = response.body()?.token ?: ""
        } else {
            Log.w(TAG, "Couldn't fetch the token")
        }
    }

    companion object {
        private const val TAG = "WeatherRepositoryImpl"

        private const val QUERY = "{me{home(id:\"ee7ce98a-a97b-4edc-9f2e-0a9c67a96f27\"){weather{minTemperature,maxTemperature,entries{time,temperature,type}}}}}"
        private const val EMAIL = "CHANGE ME"
        private const val PASSWORD = "CHANGE ME"

        private const val UNAUTHORIZED = 401
    }
}