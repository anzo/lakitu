package se.codkingkoi.lakitu.repo

import kotlinx.coroutines.flow.Flow
import se.codkingkoi.lakitu.api.WeatherData

interface WeatherRepository {
    suspend fun getWeatherData(): Flow<WeatherData?>
}