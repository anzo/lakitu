package se.codkingkoi.lakitu

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import se.codkingkoi.lakitu.api.WeatherData
import se.codkingkoi.lakitu.repo.WeatherRepository

class TemperatureViewModel(private val weatherRepo: WeatherRepository) : ViewModel() {

    private var cache: WeatherData? = null
    private val _temperatures = MutableLiveData<WeatherData?>()

    val temperatures: LiveData<WeatherData?>
        get() {
            if (cache == null) {
                viewModelScope.launch {
                    weatherRepo.getWeatherData().collect {
                        cache = it
                        _temperatures.postValue(it)
                    }
                }
            } else {
                _temperatures.value = cache
            }

            return _temperatures
        }
}