package se.codkingkoi.lakitu.api

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface BackendService {

    @POST("login.credentials")
    suspend fun getToken(@Body credentials: Credentials): Response<TokenResponse>

    @POST("gql")
    suspend fun getTemperatures(
        @Header("Authorization") token: String,
        @Body query: Query
    ): WeatherData

    data class Credentials(val email: String, val password: String)
    data class TokenResponse(val token: String)

    data class Query(val query: String)
}
