package se.codkingkoi.lakitu.api

import java.util.Date

data class WeatherData(
    val minTemperature: Float?,
    val maxTemperature: Float?,
    val entries: List<Entry>
)

data class Entry(val time: Date, val temperature: Float?, val type: String?)

/*
{
    "data": {
        "me": {
            "home": {
                "weather": {
                    "minTemperature": 1.4,
                    "maxTemperature": 1.4,
                    "entries": [
                        {
                            "time": "2020-04-28T00:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T01:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T02:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T03:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T04:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T05:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T06:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T07:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T08:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T09:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T10:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T11:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T12:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T13:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T14:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T15:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T16:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T17:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T18:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T19:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T20:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T21:00:00+02:00",
                            "temperature": null,
                            "type": null
                        },
                        {
                            "time": "2020-04-28T22:00:00+02:00",
                            "temperature": 1.4,
                            "type": "sun"
                        },
                        {
                            "time": "2020-04-28T23:00:00+02:00",
                            "temperature": 1.4,
                            "type": "sun"
                        }
                    ]
                }
            }
        }
    }
}
 */