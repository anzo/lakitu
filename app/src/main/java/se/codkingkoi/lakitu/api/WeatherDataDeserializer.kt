package se.codkingkoi.lakitu.api

import android.util.Log
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type

class WeatherDataDeserializer : JsonDeserializer<WeatherData> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): WeatherData {
        if (json == null) {
            Log.w(TAG, "No root JSON data to deserialize")
            throw JsonParseException("No root JSON data to deserialize")
        }

        val weather = json.asJsonObject["data"]
            .asJsonObject["me"]
            .asJsonObject["home"]
            .asJsonObject["weather"]
            .asJsonObject

        val minTemp = weather["minTemperature"].asFloat
        val maxTemp = weather["maxTemperature"].asFloat
        val entries = weather["entries"].asJsonArray.mapNotNull {
            context?.deserialize<Entry>(it, Entry::class.java)
        }

        return WeatherData(minTemp, maxTemp, entries)
    }

    companion object {
        private const val TAG = "WeatherDataDeserializer"
    }
}