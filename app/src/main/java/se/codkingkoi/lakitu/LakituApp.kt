package se.codkingkoi.lakitu

import android.app.Application
import com.google.gson.GsonBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import se.codkingkoi.lakitu.api.BackendService
import se.codkingkoi.lakitu.api.WeatherData
import se.codkingkoi.lakitu.api.WeatherDataDeserializer
import se.codkingkoi.lakitu.repo.WeatherRepository
import se.codkingkoi.lakitu.repo.WeatherRepositoryImpl

class LakituApp : Application() {

    private val appModule = module {

        single {
            val gson = GsonBuilder()
                .registerTypeAdapter(WeatherData::class.java, WeatherDataDeserializer())
                .create()

            val retrofit = Retrofit.Builder()
                .baseUrl("https://app.tibberdev.com/v4/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            retrofit.create(BackendService::class.java)
        }

        single<WeatherRepository> { WeatherRepositoryImpl(get()) }

        viewModel { TemperatureViewModel(get()) }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidLogger()
            androidContext(this@LakituApp)
            modules(appModule)
        }
    }
}